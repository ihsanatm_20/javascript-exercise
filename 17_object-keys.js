const car = {
    make: 'Honda',
    model: 'Accord',
    year: 2020
  }

const keys = Object.keys(car); //[]

console.log(keys);

const values = keys.map(function(key){
  return car[key];
}) 

console.log(values);

const element = Object.values(car);
console.log(element);
// const dataBaru2 = Object.keys(car);
// const nilai2 = dataBaru2.map((isi) => car[isi]);
