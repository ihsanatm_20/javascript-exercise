const a = 4;

function foo() {
    const b = a * 3;

    function bar(c) {
        const b = 2;

        console.log(a,b,c);
    }

    bar(b*4); //bar 48
}

foo();
